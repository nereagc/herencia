class Persona: 
    def __init__(self, nombre, apellido, fecha, dni):
        self.nombre= nombre
        self.apellido= apellido
        self.fecha= fecha
        self.dni= dni 

    def setNombre(self, nombre):
        self.nombre=nombre
    def getNombre(self, nombre): 
        return self.nombre

    def setApellido(self, apellido): 
        self.apellido=apellido
    def getApellido(self, apellido): 
        return self.apellido

    def setFecha(self, fecha): 
        self.fecha= fecha
    def getFecha(self, fecha): 
        return self.fecha

    def setDni(self, dni): 
        self.dni=dni
    def getDni(self, dni): 
        return self.dni
    
    def __str__(self):
        return f'nombre: {self.nombre}, apellidos: {self.apellido}, fecha de nacimiento {self.fecha}, dni: + {self.dni}'
    

class Paciente (Persona):
    def __init__(self, nombre, apellido, fecha, dni, hist):
        super().__init__(nombre, apellido, fecha, dni)
        self.historial_clinico= hist
    def ver_historial(self):
        return 'Historial Clínico: {hist}'.format(hist=self.historial_clinico)
class Medico(Persona):
    def __init__(self,nombre, apellido, fecha, dni, especi, cit):
        super().__init__(nombre, apellido, fecha, dni)
        self.especialidad= especi
        self.cita= cit
    def consultar_agenda(self):
        return 'Cita programada para el día {cit} en la especialidad de {especi}'.format(cit=self.cita, especi=self.especialidad)

persona = Persona("Karen", "Sanchez", "13/06/1999", "48887523J")
paciente= Paciente("Marta", "Pascual", "02/07/1989", "35778963P", "Cardiopatías")
medico= Medico("Sofía", "Cuadra", "19/01/1973", "56773214G", "Cardiología", "30/02/2024 a las 8:00")
gente=[persona, paciente, medico]
for people in gente: 
    print (people)
    if people== paciente: 
        print(people.ver_historial())
    elif people==medico: 
        print(people.consultar_agenda())

    
