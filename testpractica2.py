from practica2 import Persona
from practica2 import Paciente
from practica2 import Medico
import unittest

class TestHerencia(unittest.TestCase):
    def test_historial(self):
        paciente= Paciente("Marta", "Pascual", "02/07/1989", "35778963P", "Cardiopatías")
        historial= paciente.ver_historial()
        self.assertEqual(historial, "Historial Clínico: Cardiopatías")
    def  test_consultar_agenda(self):
        medico= Medico("Sofía", "Cuadra", "19/01/1973", "56773214G", "Cardiología", "30/02/2024 a las 8:00")
        agenda= medico.consultar_agenda()    
        self.assertEqual(agenda, "Cita programada para el día 30/02/2024 a las 8:00 en la especialidad de Cardiología")
if __name__ =="__main__":
    unittest.main()